<?php

require_model('cliente.php');
require_model('familia.php');
require_model('direccion_cliente.php');
require_model('articulo.php');
require_model('impuesto.php');
require_model('almacen.php');
require_model('divisa.php');
require_model('familia.php');
require_model('forma_pago.php');
require_model('impuesto.php');
require_model('serie.php');

class connection {

    public $con;
    private $dbName;

    function __construct() {

        $ruta = $_SERVER["DOCUMENT_ROOT"] . "/fs2015/plugins/importador_tpvros/datos/ventasnuevo.mdb";
        if (!file_exists($ruta)) 
        {
             die("No se ha encotrado el archivo");
        }
        $this->dbName = $ruta;
    }

    function connect() {
        $this->con = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=$this->dbName;charset=utf-8; Uid=Admin; Pwd=muertamuerta;");
        $this->con->exec("SET CHARACTER SET utf8");
        $this->con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $this->con;
    }

}

class importador_tpvros extends fs_controller {

    public $registro;
    public $agente;
    public $almacen;
    public $articulo;
    public $cliente;
    public $cliente_s;
    public $divisa;
    public $familia;
    public $forma_pago;
    public $impuesto;
    public $results;
    public $serie;
    public $tipo;
    public function __construct() {
        /* Añade el submenú "Gatos" al menú "demo" */
        parent::__construct(__CLASS__, 'Importador TPVROS', 'demo', FALSE, TRUE);
    }

    protected function process() 
    {
        $this->cliente = new cliente();
        $this->cliente_s = FALSE;
        $this->familia = new familia();
        $this->impuesto = new impuesto();
        $this->results = array();
        $this->almacen = new almacen();
        $this->serie = new serie();
        $this->forma_pago = new forma_pago();
        $this->divisa = new divisa();
        $this->custom_search = TRUE;
        if (isset($_GET['funcion'])) 
        {
            if ($_GET['funcion'] == "importar_clientes") 
            {
                $this->importar_clientes();
            } 
            elseif ($_GET['funcion'] == "importar_familias") 
            {
                $this->importar_familias();
            }
            elseif ($_GET['funcion'] == "importar_articulos") 
            {
                $this->importar_articulos();
            }
            elseif ($_GET['funcion'] == "importar_facturas") 
            {
                $this->importar_facturas();
            }
            elseif ($_GET['funcion'] == "resetear") 
            {
                $this->resetear();
            } 
            elseif ($_GET['funcion'] == "listar_clientes") 
            {
                $this->listar_clientes();
            }
            elseif ($_GET['funcion'] == "listar_familias") 
            {
                $this->listar_familias();
            }
            elseif ($_GET['funcion'] == "listar_articulos") 
            {
                $this->listar_articulos();
            }
            elseif ($_GET['funcion'] == "listar_facturas") 
            {
                $this->listar_articulos();
            }
        } 
        else {
            $this->visualizar();
        }
    }

    public function listar_clientes() {
        $sql = "SELECT clientes.cifnif, clientes.nombre, clientes.codcliente, dirclientes.direccion FROM clientes, dirclientes 
        WHERE clientes.codcliente = dirclientes.codcliente
        AND clientes.codcliente =001419";//solo voy a devolver un registro de prueba ya que es solo para comprobar
        $data = $this->db->select($sql);
        $this->template = "principal";
        if ($data) /* Se devuelven los datos de la consulta SQL */
            return $data;
        else /* Se devuelve un array vacío porque no se han devuelto datos de la consulta SQL */
            return array();
    }
    
    public function listar_familias() {
        $sql = "SELECT * FROM familias";//solo voy a devolver un registro de prueba ya que es solo para comprobar
        $data = $this->db->select($sql);
        $this->template = "listar_familias";
        if ($data) /* Se devuelven los datos de la consulta SQL */
            return $data;
        else /* Se devuelve un array vacío porque no se han devuelto datos de la consulta SQL */
            return array();
    }
    
    public function listar_articulos() 
    {
        $sql = "SELECT descripcion FORM articulos";//solo voy a devolver un registro de prueba ya que es solo para comprobar
        $data = $this->db->select($sql);
        $this->template = "listar_articulos";
        if ($data) /* Se devuelven los datos de la consulta SQL */
            return $data;
        else /* Se devuelve un array vacío porque no se han devuelto datos de la consulta SQL */
            return array();
    }

    public function resetear() {
        $this->db->exec("SET FOREIGN_KEY_CHECKS = 0");
        $this->db->exec('TRUNCATE TABLE clientes');
        $this->db->exec('TRUNCATE TABLE dirclientes');
        $this->db->exec('TRUNCATE TABLE articulos');
        $this->db->exec("SET FOREIGN_KEY_CHECKS = 1");
        $this->template = "resetear";
    }

    public function formatear_codigo($codigo) {
        if ($codigo)
            return sprintf('%06s', intval($codigo));
        else
            return NULL;
    }

    public function importar_clientes() {
        $data;
        $errordir;
        $errorclient;
        if (!ini_get('display_errors')) {
            ini_set('display_errors', '1');
        }
        try {
            //get the DB connection
            set_time_limit('600'); 
            $con = new connection();
            $pdoConnection = $con->connect();
            //query the DB
            $sql = $pdoConnection->prepare("SELECT * FROM clientes");
            $result = $sql->execute();
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $cliente = new cliente();
                $cliente->codcliente = $this->formatear_codigo($row['CodCliente']);
                $cliente->nombre = iconv('ISO-8859-1','UTF-8//TRANSLIT',$row['Nombrem']);
                $cliente->nombrecomercial = iconv('ISO-8859-1','UTF-8//TRANSLIT',$row['Nombrem']);
                $cliente->telefono1 = $row['Telefono'];
                $cliente->telefono2 = $row['movil'];
                $cliente->cifnif = $row['Nif'];
                if ($cliente->save())
                    $errorclient = 0;
                else
                    $errorclient = 1;
                $dir = new direccion_cliente();
                $dir->ciudad = iconv('ISO-8859-1','UTF-8//TRANSLIT',$row['Poblacion']);
                $dir->codcliente = $cliente->codcliente;
                $dir->codpais = 'ESP';
                $dir->codpostal = $row['CP'];
                $dir->direccion = iconv('ISO-8859-1','UTF-8//TRANSLIT',$row['Direccion']);
                $dir->domenvio = 1;
                $dir->domfacturacion = 1;
                $dir->descripcion = "Principal";
                $dir->provincia = iconv('ISO-8859-1','UTF-8//TRANSLIT',$row['Provincia']);
                if ($dir->save())
                    $errordir = 0;
                else
                    $errordir = 1;
            }
        } catch (Exception $e) {
            $this->new_message($e->getMessage());
        }
        if ($errordir == 0 && $errorclient = 0) {
            $this->new_message('Clientes agregados correctamente');
        } else {
            $this->new_message('Los clientes no se han podido agregar bien');
        }
        $this->template = "importar";
    }
    
       public function importar_familias() 
       {
        $data;
        if (!ini_get('display_errors')) {
            ini_set('display_errors', '1');
        }
        try {
            //get the DB connection
            $con = new connection();
            $pdoConnection = $con->connect();
            //query the DB
            $sql = $pdoConnection->prepare("SELECT * FROM Familias");
            $result = $sql->execute();
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                $familia = new familia();
                $familia->codfamilia = $row['codfamilia'];
                $familia->descripcion = iconv('ISO-8859-1','UTF-8//TRANSLIT',$row['Nombrem']);
                $familia->ganancia = $row['Margen1'];
                if ($familia->save())
                    $this->new_message('Familias agregados correctamente');
                else
                    $this->new_message('Familias no se han podido agregar bien');
            }
        } catch (Exception $e) {
            $this->new_message($e->getMessage());
        }
        $this->template = "importar";
    }

    public function importar_articulos() 
    {
        $data;
        $errordir;
        $errorclient;
        if (!ini_get('display_errors')) {
            ini_set('display_errors', '1');
        }
        try {
            //get the DB connection
            $con = new connection();
            $pdoConnection = $con->connect();
            //query the DB
            $sql = $pdoConnection->prepare("SELECT * FROM productos");
            $result = $sql->execute();
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) 
            {
               
               $articulo= new articulo();
               $articulo->referencia = $row['Codigo'];
               $articulo->descripcion = iconv('ISO-8859-1','UTF-8//TRANSLIT',$row['nombrem']);
               $articulo->codfamilia = $row['Familia'];;
               $articulo->controlstock = 1;
               $articulo->secompra = 1;
               $articulo->sevende = 1;
               $articulo->set_impuesto( 'IVA21' );
               $articulo->set_pvp_iva($row['PVenta1'] );
               if( $articulo->save() )
               {
                    $this->new_message("Datos del articulo modificados correctamente");
                }
                else
                    $this->new_error_msg("¡Error al guardar el articulo!");
         
            }
        } catch (Exception $e) {
            $this->new_message($e->getMessage());
        }
        $this->template = "importar";
    }
    
    
    public function importar_facturas() 
    {
        $this->agrega_series();
        $this->agrega_ejercicio();
        $data;
        $errordir;
        $errorclient;
        if (!ini_get('display_errors')) {
            ini_set('display_errors', '1');
        }
        try {
            //get the DB connection
            $con = new connection();
            $pdoConnection = $con->connect();
            //query the DB
            $sql = $pdoConnection->prepare("SELECT * FROM ventas");
            $result = $sql->execute();
            $i=0;
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) 
            {

                    $fecha_desde = strtotime('31-08-2012');
                    $fecha_fact = str_replace('/', '-',  $row['Fecha'] );
                    $fecha_fact = strtotime($fecha_fact);
                    // $fecha_otra = strtotime('31/02/2011'); daría error
                     //bool(true)!
                    

                    if ($fecha_fact > $fecha_desde) 
                    {
                        // if($row['Venta']=='T004521' || $row['Venta']=='T004687' )
                       // {
                         $continuar = TRUE;     
                         $cliente = $this->cliente->get($this->formatear_codigo($row['CodCliente']));
                        if( $cliente )
                        $this->save_codcliente( $cliente->codcliente );
                         else
                        {
                         $this->new_error_msg('Cliente no encontrado.');
                         $continuar = FALSE;
                         }
      
                      $almacen = $this->almacen->get("ALG");
                      if( $almacen )
                         $this->save_codalmacen( $almacen->codalmacen );
                      else
                      {
                         $this->new_error_msg('Almacén no encontrado.');
                         $continuar = FALSE;
                      }
                      //se ve que el numero de ejercicio te lo pone segun la fecha.
                      //automaticamente, lo tenemos que implementar pero ahora mismo no
                      /*$eje0 = new ejercicio();
                      $ejercicio = $eje0->get_by_fecha($_POST['fecha']);
                      if( $ejercicio )
                         $this->save_codejercicio( $ejercicio->codejercicio );
                      else
                      {
                         $this->new_error_msg('Ejercicio no encontrado.');
                         $continuar = FALSE;
                      }*/
                      
                      $serie = $this->serie->get(substr($row['Venta'], -7, 1));
                      if( !$serie )
                      {
                         $this->new_error_msg('Serie no encontrada.');
                         $continuar = FALSE;
                      }
                      
                      $forma_pago = $this->forma_pago->get('CONT');
                      if( $forma_pago )
                         $this->save_codpago( $forma_pago->codpago );
                      else
                      {
                         $this->new_error_msg('Forma de pago no encontrada.');
                         $continuar = FALSE;
                      }
                      
                      $divisa = $this->divisa->get("EUR");
                      if( $divisa )
                         $this->save_coddivisa( $divisa->coddivisa );
                      else
                      {
                         $this->new_error_msg('Divisa no encontrada.');
                         $continuar = FALSE;
                      }
                    if($continuar)
                    {
                        $venta= new factura_cliente();
                        $venta->codejercicio='0002';
                        $venta->codserie=$serie->codserie;
                        $cod=$venta->codejercicio.'0'.$venta->codserie.substr($row['Venta'], -6);//nos hara falta luegopara resolver el id
                        $numerofacantigua=$row['Venta'];//nos hara falta luego para hacer un select
                        $venta->codigo=$cod;
                        $venta->codagente=1;
                        $venta->codalmacen=$almacen->codalmacen;
                        $venta->codpago=$forma_pago->codpago;
                        $venta->editable=1;
                        $venta->deabono=0;
                        //extraemos la base calculamos y redondeamos el iva
                        $base = implode( '.', explode( ',', $row['Base']));
                        $iva=implode( '.', explode( ',', $row['TotalIva']));
                        $total=$base+$iva;
                        $venta->neto=$base;
                        $venta->totaliva=$iva;
                        $venta->total=$total;
                        $venta->totaleuros=$total;
                        $venta->automatica=0;

                        $dataentregafinal = implode( '-', explode( '/', $row['Fecha'] ) );
                        $venta->fecha = $dataentregafinal;
                        $venta->hora = '00:00:00';
                        $venta->coddivisa = $divisa->coddivisa;
                        foreach($cliente->get_direcciones() as $d)
                         {
                            if($d->domfacturacion)
                            {
                               $venta->codcliente = $cliente->codcliente;
                               $venta->cifnif = $cliente->cifnif;
                               $venta->nombrecliente = $cliente->nombrecomercial;
                               $venta->apartado = $d->apartado;
                               $venta->ciudad = $d->ciudad;
                               $venta->coddir = $d->id;
                               $venta->codpais = $d->codpais;
                               $venta->numero=substr($row['Venta'], -1);
                               $venta->observaciones=$row['Observaciones'];
                               $venta->codpostal = $d->codpostal;
                               $venta->direccion = $d->direccion;
                               $venta->provincia = $d->provincia;
                               break;
                            }
                         }
         
                         if( is_null($venta->codcliente) )
                         {
                            $this->new_error_msg("No hay ninguna dirección asociada al cliente.");
                         }
                        $this->new_message("la serie de esta factura es ".substr($row['Venta'], -7, 1));
                        if( $venta->save($cod) )
                        {
                             $this->new_message("La factura a sido guardada correctamente");
                             $this->agrega_lineas($numerofacantigua,$cod);
                        }
                        else
                            $this->new_error_msg("¡Error al guardar el articulo!");
                         } 
                }
              //}
            }
            
        } 
        catch (Exception $e) 
        {
            $this->new_message($e->getMessage());
        }

        $this->template = "importar";
    }

    public function agrega_lineas($numerofacantigua,$cod)
    {
        $id=$this->devuelveidfactura($cod);
        $con = new connection();
        $pdoConnection = $con->connect();
        //query the DB
        $sql = $pdoConnection->prepare("SELECT * FROM VentaDetalle WHERE Venta='".$numerofacantigua."'");
        $result = $sql->execute();
        while ($row = $sql->fetch(PDO::FETCH_ASSOC)) 
        {
            $linea = new linea_factura_cliente();
            $linea->idfactura=$id;
            $linea->referencia=$row['codArticulo'];
            $linea->descripcion=$this->empresa->var2str(iconv('ISO-8859-1','UTF-8//TRANSLIT',$row['Descripcion']));
            $linea->cantidad=$row['Cantidad'];
            $linea->codimpuesto='IVA21';
            $linea->iva=21;
            $linea->dtopor=0;
            $linea->pvpunitario = floatval($row['Pvp']);
            $linea->pvptotal = $linea->pvpunitario*$linea->cantidad;
            $linea->pvpsindto = ($linea->pvpunitario * $linea->cantidad);
                     
                     if( $linea->save() )
                     {
                        $this->new_error_msg("Linea agregada correctamente");
                     }
                     else
                     {
                        $this->new_error_msg("¡Imposible guardar la linea con referencia: ".$linea->referencia);
                     }


        }

    }

    public function devuelveidfactura($cod)
    {
        $sql="SELECT idfactura FROM facturascli WHERE codigo='".$cod."'";
        $data = $this->db->select($sql); 
        return $data[0]['idfactura']; 
    }
    
    public function agrega_series()
    {
        $serie = new serie();
        $serie->codserie = "F";
        if( !$serie->exists() )
        {
             $serie->codserie = "F";
             $serie->descripcion = 'Facturas TPVROS';
            if( $serie->save() )
            {
                $this->new_message("Datos de la serie creados correctamente");
            }
            else
                $this->new_error_msg("¡Error al guardar la serie!");
        }
        $serie = new serie();
        $serie->codserie = "T";
        if( !$serie->exists() )
        {
             $serie->codserie = "T";
             $serie->descripcion = 'Tickets TPVROS';
            if( $serie->save() )
            {
                $this->new_message("Datos de la serie creados correctamente");
            }
            else
                $this->new_error_msg("¡Error al guardar la serie!");
        }
        $serie = new serie();
        $serie->codserie = "V";
        if( !$serie->exists() )
        {
             $serie->codserie = "V";
             $serie->descripcion = 'Facturas de devolución TPVROS';
            if( $serie->save() )
            {
                $this->new_message("Datos de la serie creados correctamente");
            }
            else
                $this->new_error_msg("¡Error al guardar la serie!");
        }
        $serie = new serie();
        $serie->codserie = "N";
        if( !$serie->exists() )
        {
             $serie->codserie = "N";
             $serie->descripcion = 'Tickets de devolución TPVROS';
            if( $serie->save() )
            {
                $this->new_message("Datos de la serie creados correctamente");
            }
            else
                $this->new_error_msg("¡Error al guardar la serie!");
        }
    }

    public function agrega_ejercicio()
    {
        $ejercicio= new ejercicio();
         $ejercicio->codejercicio = "0002";
         $ejercicio->nombre = "Del 1 septiembre del 2012 al 1 de nobiembre del 2014";
         $ejercicio->fechainicio = '01-09-2012';
         $ejercicio->fechafin = '01-10-2014';
         $ejercicio->estado = 'CERRADO';
         if( $ejercicio->save() )
         {
            $this->new_message("Ejercicio ".$ejercicio->codejercicio." guardado correctamente.");
         }
         else
            $this->new_error_msg("¡Imposible guardar el ejercicio!");
    }


    public function visualizar() {
        $data;
        $errordir;
        $errorclient;
        if (!ini_get('display_errors')) {
            ini_set('display_errors', '1');
        }
        try {
            //get the DB connection
            $con = new connection();
            $pdoConnection = $con->connect();
            //query the DB
            $sql = $pdoConnection->prepare("SELECT * FROM ventas");
            $result = $sql->execute();
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                if ($row['Venta'] == "T004687" || $row['Venta']=='T004521' || $row['Venta']=='T004662') {
                        $base = implode( '.', explode( ',', $row['Base']));
                        $iva=(($base*21)/100);
                        $ivaconv= round($iva, 4, PHP_ROUND_HALF_DOWN); 
                        $ivaconv= round($ivaconv, 3, PHP_ROUND_HALF_DOWN); 
                        $ivaconv= round($ivaconv, 2, PHP_ROUND_HALF_DOWN); 

                        $total=$base+$ivaconv;
                    $this->new_message($row['Venta']."<br/>Vase sin formatear".$row['Base']."<br/> iva sin convertir:".$iva."<br/> iva convertido:".$ivaconv."<br/> base 
                      ".$base."<br/> Total ".$total."<br/>");
                }
            }
        } catch (Exception $e) {
            $this->new_message($e->getMessage());
        }
        $this->template = "visualizar";
    }

    function truncateFloat($number, $digitos)
    {
      $raiz = 10;
      $multiplicador = pow ($raiz,$digitos);
      $resultado = ((int)($number * $multiplicador)) / $multiplicador;
      return number_format($resultado, $digitos);
 
    }

}
