<?php

/*
 * This file is part of FacturaSctipts
 * Copyright (C) 2014-2015  Francisco Javier Trujillo   javier.trujillo.jimenez@gmail.com
 * Copyright (C) 2014  Carlos Garcia Gomez         neorazorx@gmail.com
 * Copyright (C) 2015  Francesc Pineda Segarra         shawe.ewahs@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_model('agente.php');
require_model('cliente.php');
require_model('proveedor.php');
require_model('detalles_garantias.php');
require_model('pais.php');
require_model('registro_garantias.php');

class clienteajax extends fs_controller
{
   public $agente;
   public $cliente;
   public $registro_garantias;
   public $detalles_garantias;
   public $busqueda;
   public $resultado;
   public $estado;
   public $proveedor;
   public $num_detalles;
   public $pais;
   public $urlplugin="plugins/clienteajax/";
   
   public function __construct()
   {
      parent::__construct(__CLASS__, 'Cliente ajax', 'ventas', FALSE, TRUE);
      /// cualquier cosa que pongas aquí se ejecutará DESPUÉS de process()
   }

   /**
    * esta función se ejecuta si el usuario ha hecho login,
    * a efectos prácticos, este es el constructor
    */
   protected function process()
   {
       
   }
}
