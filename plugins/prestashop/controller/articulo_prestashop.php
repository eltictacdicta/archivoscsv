<?php

/**
 * @author Carlos García Gómez      neorazorx@gmail.com
 * @copyright 2015, Carlos García Gómez. All Rights Reserved. 
 */

require_once 'plugins/prestashop/extra/PSWebServiceLibrary.php';
require_once 'plugins/prestashop/extra/Tools.php';
require_model('articulo.php');
require_model('articulo_propiedad.php');
require_model('impuesto.php');
require_model('tarifa.php');

/**
 * Description of articulo_prestashop
 *
 * @author carlos
 */
class articulo_prestashop extends fs_controller
{
   public $articulo;
   public $apropiedades;
   public $ps_setup;
   public $tarifa;
   
   public function __construct()
   {
      parent::__construct(__CLASS__, 'PrestaShop', 'ventas', FALSE, FALSE);
   }
   
   protected function private_core()
   {
      $this->share_extension();
      $this->tarifa = new tarifa();
      
      $fsvar = new fs_var();
      $impuesto = new impuesto();
      $ps_params = array('ps_url'=>FALSE, 'ps_ws_key'=>FALSE, 'ps_precio'=>FALSE);
      foreach($impuesto->all() as $imp)
      {
         $ps_params['ps_codimp_'.$imp->codimpuesto] = FALSE;
      }
      $this->ps_setup = $fsvar->array_get($ps_params);
      
      $this->articulo = FALSE;
      if( isset($_REQUEST['ref']) )
      {
         $art0 = new articulo();
         $this->articulo = $art0->get($_REQUEST['ref']);
      }
      
      if($this->articulo)
      {
         $ap = new articulo_propiedad();
         $this->apropiedades = $ap->array_get($this->articulo->referencia);
         
         if( !isset($this->apropiedades['ps_activo']) )
         {
            $this->apropiedades['ps_activo'] = '1';
         }
         
         if( !isset($this->apropiedades['ps_desc_corta']) )
         {
            $this->apropiedades['ps_desc_corta'] = $this->articulo->descripcion;
         }
         
         if( !isset($this->apropiedades['ps_desc_larga']) )
         {
            $this->apropiedades['ps_desc_larga'] = $this->articulo->descripcion;
         }
         
         if( !isset($this->apropiedades['ps_anchura']) )
         {
            $this->apropiedades['ps_anchura'] = 0;
            $this->apropiedades['ps_altura'] = 0;
            $this->apropiedades['ps_profundidad'] = 0;
            $this->apropiedades['ps_peso'] = 0;
            $this->apropiedades['ps_gastos_envio'] = 0;
         }
         
         if( !isset($this->apropiedades['ps_redireccion']) )
         {
            $this->apropiedades['ps_redireccion'] = '';
            $this->apropiedades['ps_redireccion_id'] = '0';
         }
         
         if( !isset($this->apropiedades['ps_precio']) )
         {
            $this->apropiedades['ps_precio'] = $this->ps_setup['ps_precio'];
            $this->apropiedades['ps_oferta'] = '';
            $this->apropiedades['ps_oferta_desde'] = '';
            $this->apropiedades['ps_oferta_hasta'] = '';
            $this->apropiedades['ps_txt_no_disponible'] = '';
         }
         
         if($this->apropiedades['ps_oferta_desde'] != '')
         {
            $this->apropiedades['ps_oferta_desde'] = date('d-m-Y', strtotime($this->apropiedades['ps_oferta_desde']));
         }
         
         if($this->apropiedades['ps_oferta_hasta'] != '')
         {
            $this->apropiedades['ps_oferta_hasta'] = date('d-m-Y', strtotime($this->apropiedades['ps_oferta_hasta']));
         }
         
         if( !isset($this->apropiedades['ps_meta_title']) )
         {
            $this->apropiedades['ps_meta_title'] = $this->articulo->descripcion;
         }
         
         if( !isset($this->apropiedades['ps_meta_keywords']) )
         {
            $this->apropiedades['ps_meta_keywords'] = '';
         }
         
         if( !isset($this->apropiedades['ps_meta_description']) )
         {
            $this->apropiedades['ps_meta_description'] = $this->articulo->descripcion;
         }
         
         if( isset($_POST['ref']) )
         {
            $this->articulo->publico = isset($_POST['publico']);
            $this->articulo->save();
            
            $this->apropiedades['ps_activo'] = '1';
            $this->apropiedades['ps_redireccion'] = '';
            $this->apropiedades['ps_redireccion_id'] = '0';
            if( !isset($_POST['activo']) )
            {
               $this->apropiedades['ps_activo'] = '0';
               $this->apropiedades['ps_redireccion'] = $_POST['redireccion'];
               $this->apropiedades['ps_redireccion_id'] = $_POST['redireccion_id'];
            }
            
            $this->apropiedades['ps_desc_corta'] = $_POST['desc_corta'];
            $this->apropiedades['ps_desc_larga'] = $_POST['desc_larga'];
            $this->apropiedades['ps_precio'] = $_POST['precio'];
            $this->apropiedades['ps_oferta'] = $_POST['oferta'];
            $this->apropiedades['ps_oferta_desde'] = $_POST['oferta_desde'];
            $this->apropiedades['ps_oferta_hasta'] = $_POST['oferta_hasta'];
            $this->apropiedades['ps_anchura'] = floatval($_POST['anchura']);
            $this->apropiedades['ps_altura'] = floatval($_POST['altura']);
            $this->apropiedades['ps_profundidad'] = floatval($_POST['profundidad']);
            $this->apropiedades['ps_peso'] = floatval($_POST['peso']);
            $this->apropiedades['ps_gastos_envio'] = floatval($_POST['gastos_envio']);
            $this->apropiedades['ps_txt_no_disponible'] = $_POST['txt_no_disponible'];
            $this->apropiedades['ps_meta_title'] = $_POST['meta_title'];
            $this->apropiedades['ps_meta_keywords'] = $_POST['meta_keywords'];
            $this->apropiedades['ps_meta_description'] = $_POST['meta_description'];
            
            if( $ap->array_save($this->articulo->referencia, $this->apropiedades) )
            {
               $this->new_message('Datos guardados correctamente.');
               $this->sync_prestashop();
            }
            else
               $this->new_error_msg('Error al guardar los datos.');
         }
      }
      else
         $this->new_error_msg('Artículo no encontrado');
   }
   
   private function share_extension()
   {
      /// añadimos la pestaña PrestaShop a las páginas de artículos
      $fsext = new fs_extension();
      $fsext->name = 'articulo_prestashop';
      $fsext->from = __CLASS__;
      $fsext->to = 'ventas_articulo';
      $fsext->type = 'tab';
      $fsext->text = '<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true" title="PrestaShop"></span>';
      $fsext->text .= '<span class="hidden-xs">&nbsp; PrestaShop</span>';
      $fsext->save();
      
      /// desactivamos el botón publicar de las páginas de artículos
      $fsext2 = new fs_extension();
      $fsext2->name = 'no_button_publicar';
      $fsext2->from = __CLASS__;
      $fsext2->to = 'ventas_articulo';
      $fsext2->type = 'config';
      $fsext2->text = 'no_button_publicar';
      $fsext2->save();
   }
   
   private function sync_prestashop()
   {
      if( !function_exists('curl_init') )
      {
         echo "No se encuentra la extensión php-curl, tienes que instalarla.\n";
      }
      else if($this->ps_setup['ps_url'])
      {
         try
         {
            $webservice = new PrestaShopWebservice($this->ps_setup['ps_url'], $this->ps_setup['ps_ws_key'], ($_POST['debug'] == 'TRUE') );
            if($this->articulo->publico)
            {
               $this->sync_articulo($webservice);
            }
            else
            {
               $this->delete_prestashop($webservice);
            }
         }
         catch(PrestaShopWebserviceException $e)
         {
            $trace = $e->getTrace();
            if($trace[0]['args'][0] == 404)
            {
               $this->new_error_msg("ERROR 404");
               
               $apx = new articulo_propiedad();
               $apx->name = 'id_prestashop';
               $apx->referencia = $this->articulo->referencia;
               $apx->delete();
            }
            else if($trace[0]['args'][0] == 401)
            {
               $this->new_error_msg("Clave de webservice incorrecta.");
            }
            else
            {
               $this->new_error_msg('ERROR: '.$e->getMessage());
            }
         }
      }
   }
   
   private function sync_articulo(&$ws)
   {
      $ap = new articulo_propiedad();
      if( !isset($this->apropiedades['id_prestashop']) )
      {
         /// creamos el artículo
         $xml = $ws->get( array('url' => $this->ps_setup['ps_url'].'/api/products?schema=blank') );
         $xml->product->reference = $this->articulo->referencia;
         $xml->product->link_rewrite->language[0] = Tools::str2url($this->articulo->descripcion);
         $xml->product->name->language[0] = $this->fix_html($this->articulo->descripcion);
         $xml->product->description->language[0] = $this->fix_html($this->apropiedades['ps_desc_larga']);
         $xml->product->description_short->language[0] = $this->fix_html($this->apropiedades['ps_desc_corta']);
         $xml->product->price = round($this->articulo->pvp_iva(), 2);
         $xml->product->condition = 'new';
         $xml->product->active = '1';
         $xml->product->indexed = '1';
         $xml->product->available_for_order = '1';
         $xml->product->show_price = '1';
         $xml->product->id_category_default = $this->get_id_category($this->articulo->codfamilia);
         $xml->product->associations->categories->category->id = $this->get_id_category($this->articulo->codfamilia);
         
         $xml2 = $ws->add( array('resource'=>'products', 'postXml'=>$xml->asXML()) );
         $this->apropiedades['id_prestashop'] = $xml2->product->id;
         $ap->array_save($this->articulo->referencia, $this->apropiedades);
      }
      
      /// modificamos el artículo
      $xml = $ws->get( array('resource'=>'products', 'id'=>$this->apropiedades['id_prestashop']) );
      unset($xml->product->manufacturer_name);
      unset($xml->product->quantity);
      $xml->product->name->language[0] = $this->fix_html($this->articulo->descripcion);
      $xml->product->description->language[0] = $this->fix_html($this->apropiedades['ps_desc_larga']);
      $xml->product->description_short->language[0] = $this->fix_html($this->apropiedades['ps_desc_corta']);
      $xml->product->active = $this->apropiedades['ps_activo'];
      $xml->product->redirect_type = $this->apropiedades['ps_redireccion'];
      $xml->product->id_product_redirected = $this->apropiedades['ps_redireccion_id'];
      $xml->product->indexed = '1';
      $xml->product->available_for_order = '1';
      $xml->product->show_price = '1';
      
      if( preg_match("/^[0-9]{1,13}$/", $this->articulo->codbarras) )
      {
         $xml->product->ean13 = $this->articulo->codbarras;
      }
      
      $xml->product->width = $this->apropiedades['ps_anchura'];
      $xml->product->height = $this->apropiedades['ps_altura'];
      $xml->product->depth = $this->apropiedades['ps_profundidad'];
      $xml->product->weight = $this->apropiedades['ps_peso'];
      $xml->product->additional_shipping_cost = $this->apropiedades['ps_gastos_envio'];
      
      /// impuesto
      if( !is_null($this->articulo->codimpuesto) )
      {
         if( isset($this->ps_setup['ps_codimp_'.$this->articulo->codimpuesto]) )
         {
            $xml->product->id_tax_rules_group = $this->ps_setup['ps_codimp_'.$this->articulo->codimpuesto];
         }
      }
      
      $desde = strtotime($this->apropiedades['ps_oferta_desde']);
      $hasta = strtotime($this->apropiedades['ps_oferta_hasta']);
      if($this->apropiedades['ps_oferta'] != '' AND $desde <= time() AND $hasta > time())
      {
         $xml->product->on_sale = '1';
         $xml->product->price = round($this->apropiedades['ps_oferta'], 2);
      }
      else
      {
         $xml->product->on_sale = '0';
         $xml->product->price = round($this->articulo->pvp_iva(), 2);
         
         if($this->apropiedades['ps_precio'] == 'pvp')
         {
            $xml->product->price = round($this->articulo->pvp, 2);
         }
         else
         {
            $tarifa = $this->tarifa->get($this->apropiedades['ps_precio']);
            if($tarifa)
            {
               $articulo = $this->articulo->get($this->articulo->referencia);
               if($articulo)
               {
                  $articulo->dtopor = 0;
                  $aux = array($articulo);
                  $tarifa->set_precios($aux);
                  $xml->product->price = round($aux[0]->pvp*(100-$aux[0]->dtopor)/100, 2);
               }
            }
         }
      }
      
      $xml->product->meta_title->language[0] = $this->apropiedades['ps_meta_title'];
      $xml->product->meta_keywords->language[0] = $this->apropiedades['ps_meta_keywords'];
      $xml->product->meta_description->language[0] = $this->apropiedades['ps_meta_description'];
      $xml->product->avaliable_later->language[0] = $this->fix_html($this->apropiedades['ps_txt_no_disponible']);
      $xml2 = $ws->edit( array('resource'=>'products', 'id'=>$this->apropiedades['id_prestashop'], 'putXml'=>$xml->asXML()) );
      
      /// modificamos el stock
      $idstock = $xml->product->associations->stock_availables->stock_available->id;
      $xml2 = $ws->get( array('resource'=>'stock_availables', 'id'=>$idstock) );
      $xml2->stock_available->quantity = (string)round( max( array(0, $this->articulo->stockfis) ) );
      $xml3 = $ws->edit( array('resource'=>'stock_availables', 'id'=>$idstock, 'putXml'=>$xml2->asXML()) );
      
      $this->articulo->imagen_url();
      $this->upload_image($this->apropiedades['id_prestashop'], $this->articulo->referencia);
   }
   
   private function fix_html($txt)
   {
      $newt = str_replace('&lt;', '<', $txt);
      $newt = str_replace('&gt;', '>', $newt);
      $newt = str_replace('&quot;', '"', $newt);
      $newt = str_replace('&#39;', "'", $newt);
      return $newt;
   }
   
   private function delete_prestashop(&$ws)
   {
      if( isset($this->apropiedades['id_prestashop']) )
      {
         $xml = $ws->delete( array('resource'=>'products', 'id'=>$this->apropiedades['id_prestashop']) );
         
         $ap = new articulo_propiedad();
         $ap->referencia = $this->articulo->referencia;
         $ap->name = 'id_prestashop';
         $ap->delete();
      }
   }
   
   private function upload_image($id, $ref)
   {
      if( file_exists('tmp/articulos/'.$ref.'.png') )
      {
         $url = 'http://'.$this->ps_setup['ps_url'].'/api/images/products/'.$id;
         
         $image_path = getcwd().'/tmp/articulos/'.$ref.'.png';
         $key = $this->ps_setup['ps_ws_key'];
         
         $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL, $url);
         curl_setopt($ch, CURLOPT_POST, TRUE);
         curl_setopt($ch, CURLOPT_USERPWD, $key.':');
         
         if( function_exists('curl_file_create') )
         {
            curl_setopt($ch, CURLOPT_POSTFIELDS, array( 'image' => curl_file_create($image_path, 'image/png', $ref.'.png') ));
         }
         else
         {
            curl_setopt($ch, CURLOPT_POSTFIELDS, array('image' => '@'.$image_path));
         }
         
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
         $result = curl_exec($ch);
         
         if($result === FALSE)
         {
            $this->new_error_msg("Curl error: ".curl_error($ch));
         }
         
         curl_close($ch);
      }
   }
   
   private function get_id_category($codfamilia)
   {
      $id = '2';
      
      $fsvar = new fs_var();
      $data = $fsvar->simple_get('ps_cat_list');
      if($data)
      {
         foreach( json_decode($data) as $d )
         {
            if($d->codfamilia == $codfamilia)
            {
               $id = (string)$d->id;
               break;
            }
         }
      }
      
      return $id;
   }
}
