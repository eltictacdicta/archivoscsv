<?php

/**
 * @author Carlos García Gómez      neorazorx@gmail.com
 * @copyright 2015, Carlos García Gómez. All Rights Reserved. 
 */

require_once 'plugins/prestashop/extra/PSWebServiceLibrary.php';
require_model('albaran.php');
require_model('almacen.php');
require_model('articulo.php');
require_model('cliente.php');
require_model('impuesto.php');
require_model('pais.php');
require_model('serie.php');
require_model('tarifa.php');

/**
 * Description of admin_prestashop
 *
 * @author carlos
 */
class admin_prestashop extends fs_controller
{
   public $almacen;
   public $articulos;
   public $estados;
   public $impuesto;
   public $pedido;
   public $pedidos;
   public $pendientes;
   public $ps_conectado;
   public $ps_setup;
   public $ps_taxes;
   public $offset;
   public $serie;
   public $tarifa;
   public $total_articulos;
   public $total_articulos_ps;
   public $total_pedidos;
   
   public function __construct()
   {
      parent::__construct(__CLASS__, 'PrestaShop', 'ventas');
   }
   
   protected function private_core()
   {
      $fsvar = new fs_var();
      $this->almacen = new almacen();
      $this->estados = array();
      $this->impuesto = new impuesto();
      $this->pedido = FALSE;
      $this->pedidos = array();
      $this->ps_conectado = FALSE;
      
      $ps_params = array('ps_url'=>FALSE, 'ps_ws_key'=>FALSE, 'ps_codalmacen'=>FALSE, 'ps_codserie'=>FALSE, 'ps_precio'=>FALSE, 'ps_max_updates'=>0);
      foreach($this->impuesto->all() as $imp)
      {
         $ps_params['ps_codimp_'.$imp->codimpuesto] = FALSE;
      }
      $this->ps_setup = $fsvar->array_get($ps_params);
      
      if(!$this->ps_setup['ps_max_updates'])
      {
         $this->ps_setup['ps_max_updates'] = 500;
      }
      
      $this->ps_taxes = array();
      $this->serie = new serie();
      $this->tarifa = new tarifa();
      $this->total_articulos = $this->count_articulos();
      $this->total_articulos_ps = 0;
      $this->total_pedidos = 0;
      
      $this->offset = 0;
      if( isset($_REQUEST['offset']) )
      {
         $this->offset = intval($_REQUEST['offset']);
      }
      
      if( isset($_POST['ps_url']) )
      {
         $this->ps_setup['ps_url'] = $_POST['ps_url'];
         
         if($_POST['ps_ws_key'] != '')
         {
            $this->ps_setup['ps_ws_key'] = $_POST['ps_ws_key'];
         }
         
         $this->ps_setup['ps_codalmacen'] = $_POST['ps_codalmacen'];
         $this->ps_setup['ps_codserie'] = $_POST['ps_codserie'];
         
         $this->ps_setup['ps_precio'] = 'pvp';
         if( isset($_POST['ps_precio']) )
         {
            $this->ps_setup['ps_precio'] = $_POST['ps_precio'];
         }
         
         foreach($this->impuesto->all() as $imp)
         {
            if( isset($_POST['ps_codimp_'.$imp->codimpuesto]) )
            {
               $this->ps_setup['ps_codimp_'.$imp->codimpuesto] = $_POST['ps_codimp_'.$imp->codimpuesto];
            }
         }
         
         if( isset($_POST['ps_max_updates']) )
         {
            $this->ps_setup['ps_max_updates'] = intval($_POST['ps_max_updates']);
         }
         
         if( $fsvar->array_save($this->ps_setup) )
         {
            $this->new_message('Datos guardados correctamente.');
         }
         else
            $this->new_error_msg('Imposible guardar los datos.');
      }
      else if( isset($_GET['reset_factualizado']) )
      {
         $this->reset_factualizado();
      }
      else if( isset($_GET['reset']) )
      {
         $this->reset_articulos();
      }
      
      if( !function_exists('curl_init') )
      {
         $this->new_error_msg('No se encuentra la extensión php-curl, tienes que instalarla.');
      }
      else if( isset($_REQUEST['id']) )
      {
         $this->template = 'pedido_prestashop';
         
         try
         {
            $webService = new PrestaShopWebservice($this->ps_setup['ps_url'], $this->ps_setup['ps_ws_key'], FALSE);
            $this->ps_conectado = TRUE;
            
            $xml2 = $webService->get( array('resource' => 'orders', 'id' => $_REQUEST['id']) );
            $this->pedido = array(
                'id' => $_REQUEST['id'],
                'fecha' => date('d-m-Y', strtotime($xml2->order->date_add)),
                'hora' => date('H:i:s', strtotime($xml2->order->date_add)),
                'total' => floatval($xml2->order->total_paid_tax_excl),
                'totaliva' => floatval($xml2->order->total_paid_tax_incl),
                'descuento' => floatval($xml2->order->total_discounts_tax_excl),
                'descuentoiva' => floatval($xml2->order->total_discounts_tax_incl),
                'gastosenvio' => floatval($xml2->order->total_shipping_tax_excl),
                'gastosenvioiva' => floatval($xml2->order->total_shipping_tax_incl),
                'referencia' => $xml2->order->reference,
                'pago' => $xml2->order->payment,
                'estado' => $this->get_order_status( intval($xml2->order->current_state), $webService ),
                'cliente' => $this->get_order_customer( intval($xml2->order->id_customer), $webService ),
                'lineas' => array(),
                'iva' => floatval($xml2->order->carrier_tax_rate),
                'direccion' => $this->get_customer_address( intval($xml2->order->id_address_invoice), $webService ),
                'idalbaran' => $this->get_albaran_id($_REQUEST['id'])
            );
            
            foreach($xml2->order->associations->order_rows->children() as $lin)
            {
               $ref = $lin->product_reference;
               if( strlen($ref) > 18 )
               {
                  $ref = substr($ref, 0, 18);
               }
               
               $this->pedido['lineas'][] = array(
                   'referencia' => $ref,
                   'descripcion' => $lin->product_name,
                   'cantidad' => floatval($lin->product_quantity),
                   'precio' => floatval($lin->unit_price_tax_excl),
                   'precioiva' => floatval($lin->unit_price_tax_incl),
               );
            }
            
            if( !$this->pedido['lineas'] )
            {
               $this->pedido['estado']['cancelado'] = TRUE;
            }
         }
         catch(PrestaShopWebserviceException $e)
         {
            $trace = $e->getTrace();
            if($trace[0]['args'][0] == 404)
            {
               $this->new_error_msg('Bad ID');
            }
            else if($trace[0]['args'][0] == 401)
            {
               $this->new_error_msg('Bad auth key');
            }
            else
            {
               $this->new_error_msg( 'Other error: '.$e->getMessage() );
            }
         }
         
         if($this->pedido)
         {
            if( isset($_GET['import']) )
            {
               if($this->pedido['idalbaran'])
               {
                  header('Location: index.php?page=ventas_albaran&id='.$this->pedido['idalbaran']);
               }
               else
                  $this->importar_pedido();
            }
         }
         else
         {
            $this->new_error_msg('Pedido no encontrado.');
         }
      }
      else if($this->ps_setup['ps_url'])
      {
         try
         {
            $webService = new PrestaShopWebservice($this->ps_setup['ps_url'], $this->ps_setup['ps_ws_key'], FALSE);
            $this->ps_conectado = TRUE;
            
            $this->ps_taxes = $this->get_taxes($webService);
            
            $xml = $webService->get( array('resource' => 'orders') );
            $this->total_pedidos = count($xml->orders->xpath('order'));
            $num = 0;
            foreach( array_reverse($xml->orders->xpath('order')) as $child )
            {
               if($num >= $this->offset AND $num < $this->offset+FS_ITEM_LIMIT)
               {
                  foreach($child->attributes() as $key => $value)
                  {
                     if($key == 'id')
                     {
                        $xml2 = $webService->get( array('resource' => 'orders', 'id' => $value) );
                        $item = array(
                            'id' => $value,
                            'fecha' => date('d-m-Y', strtotime($xml2->order->date_add)),
                            'hora' => date('H:i:s', strtotime($xml2->order->date_add)),
                            'totaliva' => floatval($xml2->order->total_paid_tax_incl),
                            'referencia' => $xml2->order->reference,
                            'pago' => $xml2->order->payment,
                            'estado' => $this->get_order_status( intval($xml2->order->current_state), $webService ),
                            'idalbaran' => $this->get_albaran_id($value)
                        );
                        
                        $this->pedidos[] = $item;
                     }
                  }
               }
               
               $num++;
            }
            
            $xml = $webService->get( array('resource' => 'products') );
            $this->total_articulos_ps = count($xml->products->xpath('product'));
         }
         catch(PrestaShopWebserviceException $e)
         {
            $trace = $e->getTrace();
            if($trace[0]['args'][0] == 404)
            {
               $this->new_error_msg('ERROR 404 ¿Has activamo mod-rewrite?');
            }
            else if($trace[0]['args'][0] == 401)
            {
               $this->new_error_msg('Clave webservice incorrecta.');
            }
            else
            {
               $this->new_error_msg( 'ERROR: '.$e->getMessage() );
               $this->new_advice('<a href="http://www.facturascripts.com/comm3/index.php?page=community_item&id=760" target="_blank">Bugs conocidos de PrestaShop</a>.');
            }
         }
         
         $art0 = new articulo();
         $this->articulos = $art0->all_publico();
      }
   }
   
   private function get_order_status($num, &$webService)
   {
      if( !isset($this->estados[$num]) )
      {
         try
         {
            $xml = $webService->get( array('resource' => 'order_states', 'id' => $num) );
            $this->estados[$num] = array(
                'texto' => $xml->order_state->name->language[0],
                'color' => $xml->order_state->color,
                'pagado' => ( intval($xml->order_state->paid) == 1 ),
                'cancelado' => ( strtolower($xml->order_state->name->language[0]) == 'cancelado' )
            );
         }
         catch(PrestaShopWebserviceException $e)
         {
            $this->estados[$num] = array(
                'texto' => 'Desconocido',
                'color' => 'white',
                'pagado' => FALSE,
                'cancelado' => TRUE
            );
         }
      }
      
      return $this->estados[$num];
   }
   
   private function get_order_customer($num, &$webService)
   {
      try
      {
         $xml = $webService->get( array('resource' => 'customers', 'id' => $num) );
         $cliente = array(
             'id' => intval($xml->customer->id),
             'nombre' => $xml->customer->firstname.' '.$xml->customer->lastname,
             'email' => $xml->customer->email
         );
      }
      catch(PrestaShopWebserviceException $e)
      {
         $cliente = FALSE;
      }
      
      return $cliente;
   }
   
   private function get_customer_address($num, &$webService)
   {
      try
      {
         $xml = $webService->get( array('resource' => 'addresses', 'id' => $num) );
         $address = array(
             'direccion' => $xml->address->address1.' '.$xml->address->address2,
             'ciudad' => $xml->address->city,
             'codpostal' => $xml->address->postcode,
             'telefono' => $xml->address->phone_mobile,
             'telefono2' => $xml->address->phone,
             'dni' => $xml->address->dni,
             'pais' => $this->get_country( intval($xml->address->id_country), $webService )
         );
      }
      catch(PrestaShopWebserviceException $e)
      {
         $address = FALSE;
      }
      
      return $address;
   }
   
   private function get_country($num, &$webService)
   {
      try
      {
         $xml = $webService->get( array('resource' => 'countries', 'id' => $num) );
         $pais = array(
             'iso' => $xml->country->iso_code,
             'nombre' => $xml->country->name->language[0]
         );
      }
      catch(PrestaShopWebserviceException $e)
      {
         $pais = FALSE;
      }
      
      return $pais;
   }
   
   private function get_albaran_id($id)
   {
      $data = $this->db->select("SELECT idalbaran FROM albaranescli WHERE numero2 = ".$this->empresa->var2str('PS#'.$id).";");
      if($data)
      {
         return $data[0]['idalbaran'];
      }
      else
         return FALSE;
   }
   
   private function importar_pedido()
   {
      $continuar = TRUE;
      
      $albaran = new albaran_cliente();
      $albaran->numero2 = 'PS#'.$this->pedido['id'];
      $albaran->fecha = $this->pedido['fecha'];
      $albaran->hora = $this->pedido['hora'];
      
      /// buscamos al cliente por email
      $data = $this->db->select("SELECT * FROM clientes WHERE email = ".$albaran->var2str($this->pedido['cliente']['email']).';');
      if($data)
      {
         $cliente = new cliente($data[0]);
         $albaran->codcliente = $cliente->codcliente;
         $albaran->cifnif = $cliente->cifnif;
         $albaran->nombrecliente = $cliente->nombrecomercial;
         $albaran->ciudad = $this->pedido['direccion']['ciudad'];
         
         /// buscamos el pais
         $pais0 = new pais();
         $pais = $pais0->get_by_iso($this->pedido['direccion']['pais']['iso']);
         if(!$pais)
         {
            $pais = new pais();
            $pais->codpais = $pais->codiso = $this->pedido['direccion']['pais']['iso'];
            $pais->nombre = $this->pedido['direccion']['pais']['nombre'];
            $pais->save();
         }
         $albaran->codpais = $pais->codpais;
         
         $albaran->codpostal = $this->pedido['direccion']['codpostal'];
         $albaran->direccion = $this->pedido['direccion']['direccion'];
      }
      else
      {
         $cliente = new cliente();
         $cliente->codcliente = $cliente->get_new_codigo();
         $cliente->cifnif = $this->pedido['direccion']['dni'];
         $cliente->nombre = $cliente->nombrecomercial = $this->pedido['cliente']['nombre'];
         $cliente->telefono1 = $this->pedido['direccion']['telefono'];
         $cliente->email = $this->pedido['cliente']['email'];
         $cliente->observaciones = 'Importado desde PrestaShop';
         
         if( $cliente->save() )
         {
            $dircliente = new direccion_cliente();
            $dircliente->codcliente = $cliente->codcliente;
            $dircliente->ciudad = $this->pedido['direccion']['ciudad'];
            $dircliente->codpostal = $this->pedido['direccion']['codpostal'];
            $dircliente->direccion = $this->pedido['direccion']['direccion'];
            $dircliente->descripcion = 'Principal';
            
            /// buscamos el pais
            $pais0 = new pais();
            $pais = $pais0->get_by_iso($this->pedido['direccion']['pais']['iso']);
            if( !$pais )
            {
               $pais = new pais();
               $pais->codpais = $pais->codiso = $this->pedido['direccion']['pais']['iso'];
               $pais->nombre = $this->pedido['direccion']['pais']['nombre'];
               $pais->save();
            }
            
            $dircliente->codpais = $pais->codpais;
            $albaran->codpais = $pais->codpais;
            
            if( $dircliente->save() )
            {
               $albaran->codcliente = $cliente->codcliente;
               $albaran->cifnif = $cliente->cifnif;
               $albaran->nombrecliente = $cliente->nombrecomercial;
               $albaran->apartado = $dircliente->apartado;
               $albaran->ciudad = $dircliente->ciudad;
               $albaran->coddir = $dircliente->id;
               $albaran->codpais = $dircliente->codpais;
               $albaran->codpostal = $dircliente->codpostal;
               $albaran->direccion = $dircliente->direccion;
               $albaran->provincia = $dircliente->provincia;
            }
            else
            {
               $this->new_error_msg("¡Imposible guardar la dirección del cliente!");
               $continuar = FALSE;
            }
         }
         else
         {
            $this->new_error_msg('Imposible guardar los datos del cliente.');
            $continuar = FALSE;
         }
      }
      
      $ej0 = new ejercicio();
      $ejercicio = $ej0->get_by_fecha($albaran->fecha);
      if($continuar AND $ejercicio)
      {
         $albaran->codejercicio = $ejercicio->codejercicio;
         
         $serie = $this->serie->get($this->ps_setup['ps_codserie']);
         if($serie)
         {
            $albaran->codserie = $serie->codserie;
            $albaran->codalmacen = $this->ps_setup['ps_codalmacen'];
            
            $div0 = new divisa();
            $divisa = $div0->get($this->empresa->coddivisa);
            if($divisa)
            {
               $albaran->coddivisa = $divisa->coddivisa;
               $albaran->tasaconv = $divisa->tasaconv;
            }
            
            $albaran->codpago = $this->empresa->codpago;
            
            if( $albaran->save() )
            {
               $this->pedido['idalbaran'] = $albaran->idalbaran;
               $art0 = new articulo();
               $portes = FALSE;
               
               foreach($this->pedido['lineas'] as $l)
               {
                  $linea = new linea_albaran_cliente();
                  $linea->idalbaran = $albaran->idalbaran;
                  $linea->referencia = $l['referencia'];
                  $linea->descripcion = $l['descripcion'];
                  $linea->cantidad = $l['cantidad'];
                  $linea->iva = $this->pedido['iva'];
                  $imp = $this->impuesto->get_by_iva($this->pedido['iva']);
                  if($imp)
                  {
                     $linea->codimpuesto = $imp->codimpuesto;
                  }
                  $linea->irpf = $albaran->irpf;
                  
                  $linea->pvpunitario = (100*$l['precioiva'])/(100+$linea->iva);
                  $linea->pvptotal = $linea->pvpsindto = $linea->pvpunitario * $linea->cantidad;
                  
                  if( $linea->save() )
                  {
                     /// descontamos del stock
                     $articulo = $art0->get($l['referencia']);
                     if($articulo)
                     {
                        $articulo->sum_stock($albaran->codalmacen, 0 - $linea->cantidad);
                     }
                     
                     $albaran->neto += $linea->pvptotal;
                     $albaran->totaliva += $linea->pvptotal * $linea->iva/100;
                     $albaran->totalirpf += ($linea->pvptotal * $linea->irpf/100);
                     $albaran->totalrecargo += ($linea->pvptotal * $linea->recargo/100);
                  }
                  else
                  {
                     $this->new_error_msg("¡Imposible guardar la linea con referencia: ".$linea->referencia);
                     $continuar = FALSE;
                  }
                  
                  if( strtolower($linea->descripcion) == 'portes' )
                  {
                     $portes = TRUE;
                  }
               }
               
               /// añadimos el descuento que pueda haber
               if($this->pedido['descuento'] != 0)
               {
                  $linea = new linea_albaran_cliente();
                  $linea->idalbaran = $albaran->idalbaran;
                  $linea->descripcion = 'Descuento';
                  $linea->cantidad = -1;
                  $linea->iva = $this->pedido['iva'];
                  $imp = $this->impuesto->get_by_iva($this->pedido['iva']);
                  if($imp)
                  {
                     $linea->codimpuesto = $imp->codimpuesto;
                  }
                  $linea->irpf = $albaran->irpf;
                  
                  $linea->pvpunitario = (100*$this->pedido['descuentoiva'])/(100+$linea->iva);
                  $linea->pvptotal = $linea->pvpsindto = $linea->pvpunitario * $linea->cantidad;
                  
                  if( $linea->save() )
                  {
                     $albaran->neto += $linea->pvptotal;
                     $albaran->totaliva += $linea->pvptotal * $linea->iva/100;
                     $albaran->totalirpf += ($linea->pvptotal * $linea->irpf/100);
                     $albaran->totalrecargo += ($linea->pvptotal * $linea->recargo/100);
                  }
                  else
                  {
                     $this->new_error_msg("¡Imposible guardar la linea de descuento.");
                     $continuar = FALSE;
                  }
               }
               
               /// añadimos los gastos de envío que pueda haber
               if($this->pedido['gastosenvio'] != 0 AND !$portes)
               {
                  $linea = new linea_albaran_cliente();
                  $linea->idalbaran = $albaran->idalbaran;
                  $linea->descripcion = 'Transporte';
                  $linea->cantidad = 1;
                  $linea->iva = $this->pedido['iva'];
                  $imp = $this->impuesto->get_by_iva($this->pedido['iva']);
                  if($imp)
                  {
                     $linea->codimpuesto = $imp->codimpuesto;
                  }
                  $linea->irpf = $albaran->irpf;
                  
                  $linea->pvpunitario = (100*$this->pedido['gastosenvioiva'])/(100+$linea->iva);
                  $linea->pvptotal = $linea->pvpsindto = $linea->pvpunitario * $linea->cantidad;
                  
                  if( $linea->save() )
                  {
                     $albaran->neto += $linea->pvptotal;
                     $albaran->totaliva += $linea->pvptotal * $linea->iva/100;
                     $albaran->totalirpf += ($linea->pvptotal * $linea->irpf/100);
                     $albaran->totalrecargo += ($linea->pvptotal * $linea->recargo/100);
                  }
                  else
                  {
                     $this->new_error_msg("¡Imposible guardar la linea de gastos de envio.");
                     $continuar = FALSE;
                  }
               }
               
               if($continuar)
               {
                  /// redondeamos
                  $albaran->neto = round($albaran->neto, FS_NF0);
                  $albaran->totaliva = round($albaran->totaliva, FS_NF0);
                  $albaran->totalirpf = round($albaran->totalirpf, FS_NF0);
                  $albaran->totalrecargo = round($albaran->totalrecargo, FS_NF0);
                  $albaran->total = $albaran->neto + $albaran->totaliva - $albaran->totalirpf + $albaran->totalrecargo;
                  
                  if( $albaran->save() )
                  {
                     $this->new_message("<a href='".$albaran->url()."'>".FS_ALBARAN."</a> generado correctamente.");
                     
                     if( abs($albaran->total - $this->pedido['totaliva']) >= 0.01 )
                     {
                        $this->new_error_msg('Los totales no coinciden.');
                     }
                  }
                  else
                  {
                     $this->new_error_msg("¡Imposible actualizar el <a href='".$albaran->url()."'>".FS_ALBARAN."</a>!");
                  }
               }
               else if( !$albaran->delete() )
               {
                  $this->new_error_msg("¡Imposible eliminar el <a href='".$albaran->url()."'>".FS_ALBARAN."</a>!");
               }
            }
            else
            {
               $this->new_error_msg('Error al guardar los datos del '.FS_ALBARAN);
            }
         }
         else
         {
            $this->new_error_msg('Imposible encontrar la serie para el '.FS_ALBARAN);
         }
      }
   }
   
   public function count_articulos()
   {
      $data = $this->db->select("SELECT COUNT(referencia) as total FROM articulos WHERE publico = true;");
      if($data)
      {
         return intval($data[0]['total']);
      }
      else
         return 0;
   }
   
   private function reset_factualizado()
   {
      if( $this->db->exec("DELETE FROM articulo_propiedades WHERE name = 'ps_factualizado';") )
      {
         $this->new_message('Fechas reseteadas correctamente.');
         
         $fslog = new fs_log();
         $fslog->tipo = 'warning';
         $fslog->detalle = 'Se han reseteado las fechas de los artículos de prestashop.';
         $fslog->usuario = $this->user->nick;
         $fslog->ip = $_SERVER['REMOTE_ADDR'];
         $fslog->save();
      }
      else
         $this->new_error_msg('Error al resetear las fechas.');
   }
   
   private function reset_articulos()
   {
      if( $this->db->exec("DELETE FROM articulo_propiedades WHERE name = 'id_prestashop' OR name = 'ps_factualizado';") )
      {
         $this->new_message('Artículos reseteados correctamente.');
         
         $fslog = new fs_log();
         $fslog->tipo = 'warning';
         $fslog->detalle = 'Se han reseteado los artículos de prestashop.';
         $fslog->usuario = $this->user->nick;
         $fslog->ip = $_SERVER['REMOTE_ADDR'];
         $fslog->save();
      }
      else
         $this->new_error_msg('Error al resetear los artículos.');
   }
   
   private function get_taxes(&$ws)
   {
      $taxes = $this->cache->get_array('ps_tax_rule_groups');
      if(!$taxes)
      {
         $xml = $ws->get( array('resource' => 'tax_rule_groups') );
         if($xml)
         {
            foreach($xml->tax_rule_groups->xpath('tax_rule_group') as $child)
            {
               foreach($child->attributes() as $key => $value)
               {
                  if($key == 'id')
                  {
                     $xml2 = $ws->get( array('resource' => 'tax_rule_groups', 'id' => $value) );
                     if($xml2)
                     {
                        $taxes[] = array(
                            'id' => (string)$value,
                            'name' => (string)$xml2->tax_rule_group->name
                        );
                     }
                  }
               }
            }
         }
         $this->cache->set('ps_tax_rule_groups', $taxes, 300);
      }
      
      return $taxes;
   }
}
