# Prestashop #

Permite sincronizar artículos de FacturaScripts a PrestaShop, además, los pedidos
de PrestaShop se muestran en el menú ventas y se permite su importación (pedido y cliente).


NOTAS:

Se debe corregir un bug en prestshop para su correcto funcionamiento.

Editar el archivo classes/order/Order.php y cambiar la función getWsCurrentState() quitando el parámetro $state

-	public function getWsCurrentState($state)
+	public function getWsCurrentState()

https://github.com/PrestaShop/PrestaShop/pull/2404/files
