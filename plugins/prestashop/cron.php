<?php

/**
 * @author Carlos García Gómez      neorazorx@gmail.com
 * @copyright 2015, Carlos García Gómez. All Rights Reserved. 
 */

require_once 'plugins/prestashop/extra/PSWebServiceLibrary.php';
require_once 'plugins/prestashop/extra/Tools.php';
require_model('articulo.php');
require_model('articulo_propiedad.php');
require_model('familia.php');
require_model('impuesto.php');
require_model('tarifa.php');

/**
 * Description of cron
 *
 * @author carlos
 */
class prestashop_cron
{
   private $categorias;
   private $db;
   private $line_code;
   private $line_item;
   private $ps_setup;
   private $tarifa;
   private $webservice;
   
   public function __construct(&$db)
   {
      $tiempo = explode(' ', microtime());
      $uptime = $tiempo[1] + $tiempo[0];
      
      $this->db = $db;
      $this->line_code = 0;
      $this->line_item = '';
      
      $fsvar = new fs_var();
      $impuesto = new impuesto();
      $ps_params = array('ps_url'=>FALSE, 'ps_ws_key'=>FALSE, 'ps_precio'=>FALSE, 'ps_max_updates'=>FALSE);
      foreach($impuesto->all() as $imp)
      {
         $ps_params['ps_codimp_'.$imp->codimpuesto] = FALSE;
      }
      $this->ps_setup = $fsvar->array_get($ps_params);
      
      if(!$this->ps_setup['ps_max_updates'])
      {
         $this->ps_setup['ps_max_updates'] = 500;
      }
      
      $this->categorias = array();
      $this->tarifa = new tarifa();
      $this->webservice = FALSE;
      
      if( !function_exists('curl_init') )
      {
         echo "No se encuentra la extension php-curl, tienes que instalarla.\n";
      }
      else if($this->ps_setup['ps_url'])
      {
         try
         {
            $this->webservice = new PrestaShopWebservice($this->ps_setup['ps_url'], $this->ps_setup['ps_ws_key'], FALSE);
            $this->sync_familias();
            $this->sync_articulos();
         }
         catch(PrestaShopWebserviceException $e)
         {
            $trace = $e->getTrace();
            if($trace[0]['args'][0] == 404)
            {
               echo 'Linea '.$this->line_code.". Item: ".$this->line_item.". Bad ID\n";
            }
            else if($trace[0]['args'][0] == 401)
            {
               echo 'Linea '.$this->line_code.". Item: ".$this->line_item.". Bad auth key\n";
            }
            else
            {
               echo 'Linea '.$this->line_code.". Item: ".$this->line_item.". Other error: ".$e->getMessage()."\n";
            }
         }
      }
      
      /// eliminamos los id_prestashop de los artículos que no son publicos
      $this->db->exec("DELETE FROM articulo_propiedades where name = 'id_prestashop' AND referencia NOT IN (SELECT referencia FROM articulos WHERE publico = true);");
      
      $tiempo = explode(' ', microtime());
      echo "\nTiempo de ejecucion: ".number_format($tiempo[1] + $tiempo[0] - $uptime, 3)." s\n";
   }
   
   private function sync_familias()
   {
      echo "Comprobando categorias...";
      
      /// obtenemos las categorias de prestashop
      $this->line_code = 100;
      $xml = $this->webservice->get( array('resource'=>'categories') );
      
      /// obtenemos las familias publicas
      $familias = array();
      $data = $this->db->select("SELECT * FROM familias WHERE codfamilia IN (SELECT codfamilia FROM articulos WHERE publico = true);");
      if($data)
      {
         foreach($data as $d)
            $familias[] = new familia($d);
         
         /// ahora comprobamos las madres
         $fam0 = new familia();
         for($i=0; $i<count($familias); $i++)
         {
            if( !is_null($familias[$i]->madre) )
            {
               $encontrada = FALSE;
               foreach($familias as $fam2)
               {
                  if($fam2->codfamilia == $familias[$i]->madre)
                  {
                     $encontrada = TRUE;
                     break;
                  }
               }
               
               if(!$encontrada)
               {
                  $famx = $fam0->get($familias[$i]->madre);
                  if($famx)
                  {
                     $familias[] = $famx;
                  }
               }
            }
         }
      }
      
      $fsvar = new fs_var();
      $data = $fsvar->simple_get('ps_cat_list');
      if($data)
      {
         foreach( json_decode($data) as $d )
         {
            foreach($xml->categories->xpath('category') as $cat)
            {
               foreach($cat->attributes() as $key => $value)
               {
                  if($key == 'id')
                  {
                     if( (string)$d->id == (string)$value )
                     {
                        foreach($familias as $fam)
                        {
                           if( $fam->codfamilia == (string)$d->codfamilia )
                           {
                              /// solamente añadimos a la lista de relaciones las categorías que estén en prestashop y en familias
                              $this->categorias[] = array(
                                  'id' => (string)$d->id,
                                  'codfamilia' => (string)$d->codfamilia,
                              );
                              break;
                           }
                        }
                     }
                  }
               }
            }
         }
      }
      
      
      foreach( array_reverse($xml->categories->xpath('category')) as $cat )
      {
         foreach($cat->attributes() as $key => $value)
         {
            if($key == 'id')
            {
               ///echo (string)$value." ";
               
               $encontrada = FALSE;
               foreach($this->categorias as $cat)
               {
                  if($cat['id'] == (string)$value)
                  {
                     $encontrada = TRUE;
                     break;
                  }
               }
               
               if( !$encontrada AND intval($value) > 2 )
               {
                  /// eliminamos las categorias que no están en la lista de relaciones
                  $this->line_code = 194;
                  $this->line_item = (string)$value;
                  $this->webservice->delete( array('resource'=>'categories', 'id'=>(string)$value) );
                  echo 'D ';
               }
            }
         }
      }
      
      echo "\nSincronizando familias...";
      foreach($familias as $fam)
      {
         ///echo $fam->codfamilia;
         
         $encontrada = FALSE;
         $madre_encontrada = is_null($fam->madre);
         foreach($this->categorias as $cat)
         {
            if($cat['codfamilia'] == $fam->codfamilia)
            {
               $encontrada = TRUE;
               echo '.';
            }
            else if($cat['codfamilia'] == $fam->madre)
            {
               $madre_encontrada = TRUE;
            }
         }
         if( !$encontrada AND $madre_encontrada )
         {
            /// creamos la categoria
            $this->line_code = 225;
            $xml = $this->webservice->get( array('url' => $this->ps_setup['ps_url'].'/api/categories?schema=blank') );
            unset($xml->category->id);
            unset($xml->category->position);
            unset($xml->category->id_shop_default);
            unset($xml->category->date_add);
            unset($xml->category->date_upd);
            $xml->category->id_parent = $this->get_id_category($fam->madre);
            $xml->category->is_root_category = '0';
            $xml->category->active = '1';
            $xml->category->link_rewrite->language[0] = Tools::str2url($fam->descripcion);
            $xml->category->name->language[0] = $this->fix_html($fam->descripcion);
            $xml->category->description->language[0] = $this->fix_html($fam->descripcion);
            
            $this->line_code = 239;
            $this->line_item = $fam->codfamilia;
            $xml2 = $this->webservice->add( array('resource'=>'categories', 'postXml'=>$xml->asXML()) );
            if($xml2)
            {
               echo '+';
               
               $this->categorias[] = array(
                   'id' => (string)$xml2->category->id,
                   'codfamilia' => $fam->codfamilia,
               );
            }
         }
      }
      
      $fsvar->simple_save('ps_cat_list', json_encode($this->categorias) );
      
      echo "\n";
   }
   
   private function sync_articulos()
   {
      echo "Sincronizamos articulos...";
      
      $articulo = new articulo();
      $articulo_prop = new articulo_propiedad();
      $offset = 0;
      
      /// eliminamos de prestashop los artículos que no están en FacturaScripts
      $this->line_code = 268;
      $this->line_item = '';
      $xml = $this->webservice->get( array('resource'=>'products') );
      foreach($xml->products->children() as $child)
      {
         foreach($child->attributes() as $key => $value)
         {
            if($key == 'id')
            {
               $sql = "SELECT * FROM articulos WHERE referencia IN (SELECT referencia FROM articulo_propiedades"
                       . " WHERE name = 'id_prestashop' AND text = ".$articulo->var2str($value).");";
               $data = $this->db->select($sql);
               if($data)
               {
                  $art = new articulo($data[0]);
                  if(!$art->publico)
                  {
                     $this->line_code = 285;
                     $this->line_item = (string)$value;
                     $this->webservice->delete( array('resource'=>'products', 'id'=>$value) );
                     echo 'D';
                  }
               }
               else
               {
                  $this->line_code = 293;
                  $this->line_item = (string)$value;
                  $this->webservice->delete( array('resource'=>'products', 'id'=>$value) );
                  echo 'D';
               }
            }
         }
      }
      
      /// sincronizamos los artículos de FacturaScripts a PrestaShop
      $max_updates = intval($this->ps_setup['ps_max_updates']);
      $articulos = $this->articulos_publicos($offset);
      while( count($articulos) > 0 AND $max_updates > 0 )
      {
         foreach($articulos as $art)
         {
            $this->line_item = $art->referencia;
            $nuevo = FALSE;
            $aprops = $articulo_prop->array_get($art->referencia);
            
            echo $art->referencia;
            
            if( !isset($aprops['id_prestashop']) )
            {
               /// creamos el artículo
               $this->line_code = 318;
               $xml = $this->webservice->get( array('url' => $this->ps_setup['ps_url'].'/api/products?schema=blank') );
               $xml->product->reference = $art->referencia;
               $xml->product->link_rewrite->language[0] = Tools::str2url($art->descripcion);
               $xml->product->name->language[0] = $this->fix_html($art->descripcion);
               $xml->product->description->language[0] = $this->fix_html($art->descripcion);
               $xml->product->description_short->language[0] = $this->fix_html($art->descripcion);
               $xml->product->price = round((string)$art->pvp_iva(), 2);
               $xml->product->condition = 'new';
               $xml->product->active = '1';
               $xml->product->indexed = '1';
               $xml->product->available_for_order = '1';
               $xml->product->show_price = '1';
               $xml->product->id_category_default = '2';
               $xml->product->associations->categories->category->id = '2';
               
               $this->line_code = 334;
               
               $xml2 = $this->webservice->add( array('resource'=>'products', 'postXml'=>$xml->asXML()) );
               $aprops['id_prestashop'] = $xml2->product->id;
               $articulo_prop->array_save($art->referencia, $aprops);
               echo '+';
               
               $nuevo = TRUE;
            }
            
            if( !isset($aprops['ps_activo']) )
            {
               $aprops['ps_activo'] = '1';
            }
            
            if( !isset($aprops['ps_desc_larga']) )
            {
               $aprops['ps_desc_larga'] = $art->descripcion;
            }
            
            if( !isset($aprops['ps_desc_corta']) )
            {
               $aprops['ps_desc_corta'] = $art->descripcion;
            }
            if( !isset($aprops['ps_anchura']) )
            {
               $aprops['ps_anchura'] = 0;
               $aprops['ps_altura'] = 0;
               $aprops['ps_profundidad'] = 0;
               $aprops['ps_peso'] = 0;
               $aprops['ps_gastos_envio'] = 0;
            }
            
            if( !isset($aprops['ps_redireccion']) )
            {
               $aprops['ps_redireccion'] = '';
               $aprops['ps_redireccion_id'] = '0';
            }
            
            if( !isset($aprops['ps_precio']) )
            {
               $aprops['ps_precio'] = $this->ps_setup['ps_precio'];
               $aprops['ps_oferta'] = '';
               $aprops['ps_oferta_desde'] = '';
               $aprops['ps_oferta_hasta'] = '';
               $aprops['ps_txt_no_disponible'] = '';
            }
            
            if( !isset($aprops['ps_factualizado']) )
            {
               $aprops['ps_factualizado'] = Date('d-m-Y');
            }
            
            /// modificamos el artículo
            $this->line_code = 388;
            $xml = $this->webservice->get( array('resource'=>'products', 'id'=>$aprops['id_prestashop']) );
            if( $nuevo OR strtotime($xml->product->date_upd) < strtotime($aprops['ps_factualizado']) )
            {
               unset($xml->product->manufacturer_name);
               unset($xml->product->quantity);
               $xml->product->name->language[0] = $this->fix_html($art->descripcion);
               $xml->product->description->language[0] = $this->fix_html($aprops['ps_desc_larga']);
               $xml->product->description_short->language[0] = $this->fix_html($aprops['ps_desc_corta']);
               $xml->product->active = $aprops['ps_activo'];
               $xml->product->redirect_type = $aprops['ps_redireccion'];
               $xml->product->id_product_redirected = $aprops['ps_redireccion_id'];
               $xml->product->indexed = '1';
               $xml->product->available_for_order = '1';
               $xml->product->show_price = '1';
               $xml->product->id_category_default = $this->get_id_category($art->codfamilia);
               $xml->product->associations->categories->category->id = $this->get_id_category($art->codfamilia);
               
               if( preg_match("/^[0-9]{1,13}$/", $art->codbarras) )
               {
                  $xml->product->ean13 = $art->codbarras;
               }
               
               $xml->product->width = $aprops['ps_anchura'];
               $xml->product->height = $aprops['ps_altura'];
               $xml->product->depth = $aprops['ps_profundidad'];
               $xml->product->weight = $aprops['ps_peso'];
               $xml->product->additional_shipping_cost = $aprops['ps_gastos_envio'];
               
               /// impuesto
               if( !is_null($art->codimpuesto) )
               {
                  if( isset($this->ps_setup['ps_codimp_'.$art->codimpuesto]) )
                  {
                     $xml->product->id_tax_rules_group = $this->ps_setup['ps_codimp_'.$art->codimpuesto];
                  }
               }
               
               $desde = strtotime($aprops['ps_oferta_desde']);
               $hasta = strtotime($aprops['ps_oferta_hasta']);
               if($aprops['ps_oferta'] != '' AND $desde <= time() AND $hasta > time())
               {
                  $xml->product->on_sale = '1';
                  $xml->product->price = round($aprops['ps_oferta'], 2);
               }
               else
               {
                  $xml->product->on_sale = '0';
                  $xml->product->price = round($art->pvp_iva(), 2);
                  
                  if($aprops['ps_precio'] == 'pvp')
                  {
                     $xml->product->price = round($art->pvp, 2);
                  }
                  else
                  {
                     $tarifa = $this->tarifa->get($aprops['ps_precio']);
                     if($tarifa)
                     {
                        $articulo2 = $art->get($art->referencia);
                        if($articulo2)
                        {
                           $articulo2->dtopor = 0;
                           $aux = array($articulo2);
                           $tarifa->set_precios($aux);
                           $xml->product->price = round($aux[0]->pvp*(100-$aux[0]->dtopor)/100, 2);
                        }
                     }
                  }
               }
               
               $xml->product->avaliable_later->language[0] = $this->fix_html($aprops['ps_txt_no_disponible']);
               $this->line_code = 454;
               $xml2 = $this->webservice->edit( array('resource'=>'products', 'id'=>$aprops['id_prestashop'], 'putXml'=>$xml->asXML()) );
               
               /// modificamos el stock
               $idstock = $xml->product->associations->stock_availables->stock_available->id;
               $this->line_code = 459;
               $xml2 = $this->webservice->get( array('resource'=>'stock_availables', 'id'=>$idstock) );
               $xml2->stock_available->quantity = (string)round( max( array(0, $art->stockfis) ) );
               $this->line_code = 462;
               $xml3 = $this->webservice->edit( array('resource'=>'stock_availables', 'id'=>$idstock, 'putXml'=>$xml2->asXML()) );
               
               $art->imagen_url();
               $this->upload_image($aprops['id_prestashop'], $art->referencia);
            }
            
            $articulo_prop->array_save($art->referencia, $aprops);
            echo '.';
            
            $offset++;
            $max_updates--;
         }
         
         $articulos = $this->articulos_publicos($offset);
      }
   }
   
   private function get_id_category($codfamilia)
   {
      $id = '2';
      
      foreach($this->categorias as $cat)
      {
         if($cat['codfamilia'] == $codfamilia)
         {
            $id = $cat['id'];
            break;
         }
      }
      
      return $id;
   }
   
   private function upload_image($id, $ref)
   {
      if( file_exists('tmp/articulos/'.$ref.'.png') )
      {
         $url = 'http://'.$this->ps_setup['ps_url'].'/api/images/products/'.$id;
         
         $image_path = getcwd().'/tmp/articulos/'.$ref.'.png';
         $key = $this->ps_setup['ps_ws_key'];
         
         $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL, $url);
         curl_setopt($ch, CURLOPT_POST, TRUE);
         curl_setopt($ch, CURLOPT_USERPWD, $key.':');
         
         if( function_exists('curl_file_create') )
         {
            curl_setopt($ch, CURLOPT_POSTFIELDS, array( 'image' => curl_file_create($image_path, 'image/png', $ref.'.png') ));
         }
         else
         {
            curl_setopt($ch, CURLOPT_POSTFIELDS, array('image' => '@'.$image_path));
         }
         
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
         $this->line_code = 520;
         $result = curl_exec($ch);
         
         if($result === FALSE)
         {
            echo "\nCurl error: ".curl_error($ch)."\n";
         }
         else
         {
            echo '_I';
         }
         
         curl_close($ch);
      }
   }
   
   private function fix_html($txt)
   {
      $newt = str_replace('&lt;', '<', $txt);
      $newt = str_replace('&gt;', '>', $newt);
      $newt = str_replace('&quot;', '"', $newt);
      $newt = str_replace('&#39;', "'", $newt);
      return $newt;
   }
   
   private function articulos_publicos($offset = 0)
   {
      $alist = array();
      
      $sql = "SELECT * FROM articulos WHERE publico AND referencia NOT IN"
              . " (SELECT referencia FROM articulo_propiedades WHERE name = 'ps_factualizado' AND text = '".date('d-m-Y')."')";
      $data = $this->db->select_limit($sql, FS_ITEM_LIMIT, $offset);
      if($data)
      {
         foreach($data as $d)
            $alist[] = new articulo($d);
      }
      
      return $alist;
   }
}

new prestashop_cron($db);