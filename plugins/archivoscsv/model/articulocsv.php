<?php
/*
 * This file is part of FacturaSctipts
 * Copyright (C) 2013-2015  Carlos Garcia Gomez  neorazorx@gmail.com
 * Forked Francisco Javier Trujillo Jimenez
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*require_model('albaran_cliente.php');
require_model('albaran_proveedor.php');
require_model('familia.php');
require_model('impuesto.php');
require_model('stock.php');*/

/**
 * Almacena los datos de un artículos.
 */
class articulocsv extends fs_model
{
   /**
    * Clave primaria. Varchar (18).
    * @var type 
    */
   public $archivo;
   public $exists;
   
   /**
    * Define el tipo de artículo, así se pueden establecer distinciones
    * según un tipo u otro.
    * @var type Varchar (10).
    */
   /*public $cat_web;
   public $url_imagen;
   public $descripcion_larga;
   private $exists;
   public $sql;*/


   public function __construct($a=FALSE)
   {
      parent::__construct('articuloscsv', 'plugins/archivoscsv/');
      
   }
   
   protected function install()
   {
      /* la tabla articulos tiene claves ajeas a familias, impuestos y stocks
      new familia();
      new impuesto();*/
      
      
      
      return '';
   }
   
   public function get_descripcion_64()
   {
      return base64_encode($this->descripcion);
   }
   
   
   public function get($ref)
   {
       
   }
   
   
   /**
    * Esta función devuelve TRUE si el artículo ya existe en la base de datos.
    * Por motivos de rendimiento y al ser esta una clase de uso intensivo,
    * se utiliza la variable $this->exists para almacenar el resultado.
    * @return type
    */
   public function exists()
   {
      /*if( !$this->exists )
      {
         if( $this->db->select("SELECT referencia FROM ".$this->table_name." WHERE referencia = ".$this->var2str($this->referencia).";") )
         {
            $this->exists = TRUE;
         }
      }
      
      return $this->exists;*/
   }
   
   public function test()
   {
      $status = FALSE;
      
       return true;
   }
   
   public function save()
   {
      if( $this->test() )
      {
         
         /*if( $this->exists() )
         {
            $sql = "UPDATE ".$this->table_name." SET descripcion_larga = ".$this->var2str($this->descripcion_larga).
                    ", url_imagen = ".$this->var2str($this->url_imagen).
                    ", cat_web = ".$this->var2str($this->cat_web).
                    " WHERE referencia = ".$this->var2str($this->referencia).";";
         }
         else
         {
            $sql = "";
         }*/
         $sql="LOAD DATA INFILE '".$this->archivo."'
                INTO TABLE ".$this->table_name."
                FIELDS TERMINATED BY ';'
                ENCLOSED BY '\"'
                ESCAPED BY '\\'
                LINES TERMINATED BY '\r\n'";
         
         if( $this->db->exec($sql) )
         {
            $this->exists = TRUE;
            return TRUE;
         }
         else
            return FALSE;
      }
      else
         return FALSE;
   }
   public function cargaarchivo($archivo)
   {
       $this->archivo=$archivo;
   }
   public function delete()
   {
//      $this->clean_cache();
//      $this->clean_image_cache();
      
      /*$sql = "DELETE FROM ".$this->table_name." WHERE referencia = ".$this->var2str($this->referencia).";";
      if( $this->db->exec($sql) )
      {
         $this->exists = FALSE;
         return TRUE;
      }
      else
         return FALSE;*/
   }  

}
